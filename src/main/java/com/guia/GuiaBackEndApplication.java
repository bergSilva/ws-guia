package com.guia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuiaBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuiaBackEndApplication.class, args);
	}

}
