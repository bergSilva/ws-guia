package com.guia.modelo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "state")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sta_id")
    private Long id;
    @Column(name = "sta_name")
    private String name;
    @Column(name = "sta_pais")
    private String pais;
    @Column(name = "sta_sigla")
    private String initials;
}
