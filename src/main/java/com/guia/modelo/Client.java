package com.guia.modelo;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "address")
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "add_id")
    private Long id;

    @Column(name = "add_street")
    private String street;

    @Column(name = "add_neighborhood")
    private String eighborhood;

    @Column(name = "add_number")
    private String number;

    @Column(name = "add_city")
    private String city;

    @Column(name = "add_state")
    private String state;

    @Column(name = "add_lat")
    private String latitude;

    @Column(name = "add_lng")
    private String longitude;


}
