package com.guia.modelo;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "package")
public class Package implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pac_id`")
    private Long id;

    @Column(name = "pac_name")
    private String name;

    @Column(name = "pac_time")
    private String time;

    @Column(name = "pac_km")
    private String km;

    @Column(name = "pac_price")
    private String price;


}
