package com.guia.modelo;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "guide")
public class Guide implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gui_id`")
    private Long id;

    @Column(name = "gui_name")
    private String name;

    @Column(name = "gui_cpf")
    private String cpf;

    @Column(name = "gui_cnh")
    private String cnh;

    @Column(name = "gui_birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;

    @Column(name = "gui_idade")
    @Transient
    private String idade;

    @Column(name = "gui_email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;


}
