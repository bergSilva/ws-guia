package com.guia.modelo;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "car")
public class Car implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id`")
    private Long id;

    @Column(name = "car_model")
    private String street;

    @Column(name = "car_brand")
    private String eighborhood;

    @Column(name = "car_brand")
    private String number;

    @Column(name = "car_year")
    private String city;

    @Column(name = "car_board")
    private String state;

    @Column(name = "car_document")
    private String latitude;


}
