package com.guia.modelo;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "script")
public class Script implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "scr_id`")
    private Long id;

    @Column(name = "scr_date")
    private String date;

    @Column(name = "scr_time")
    private String time;

    @Column(name = "scr_boarding")
    private String boarding;
    
    @Column(name = "scr_peoplenumber")
    private Integer peoplenumber;

    @ManyToOne
    @JoinColumn(name ="client_cli_id" )
    private Client client;

    @ManyToOne
    @JoinColumn(name = "guide_gui_id")
    private Guide guide;

    @ManyToOne
    @JoinColumn(name ="package_pac_id" )
    private Package aPackage;


}
