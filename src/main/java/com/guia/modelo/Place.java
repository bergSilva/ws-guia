package com.guia.modelo;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "place")
public class Place implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pla_id`")
    private Long id;

    @Column(name = "pla_name")
    private String name;

    @Column(name = "pla_type")
    private String type;


    @ManyToOne
    @JoinColumn(name ="package_pac_id" )
    private Package aPackage;

    @ManyToOne
    @JoinColumn(name = "address_add_id")
    private  Address address;


}
